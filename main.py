from src.variable import Variable

if __name__ == "__main__":
    a = Variable(5)
    s = Variable(2)
    p = s * 2
    b = a + p
    c = b * 2
    r = c + 3
    d = r ** 2
    e = d * 3
    k = e.relu()

    print(e)
    k.backward()
    print(a.grad)
    print(s.grad)
