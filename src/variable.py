import math


def convert_input(func):
    def wrapper(*args, **kwargs):
        variable_input = args[1] if isinstance(args[1], Variable) else Variable(args[1], grad=0, op="const")
        return func(args[0], variable_input, *args[2:], **kwargs)
    return wrapper


class Variable:
    def __init__(self, value, grad=1, op="var"):
        self.op = op
        self.value = value
        self.grad = grad
        self.prev = []
        self.next = []
        self.derivative = None

    @convert_input
    def __add__(self, other):
        rv = Variable(self.value + other.value, op="add")
        rv.prev.extend([self, other])
        self.next.append(rv)
        rv.derivative = lambda x: x
        return rv

    @convert_input
    def __sub__(self, other):
        rv = Variable(self.value - other.value, op="sub")
        rv.prev.extend([self, other])
        self.next.append(rv)
        rv.derivative = lambda x: x
        return rv

    @convert_input
    def __mul__(self, other):
        rv = Variable(self.value * other.value, op="mull")
        rv.prev.extend([self, other])
        self.next.append(rv)
        rv.derivative = lambda x: x * other.value
        return rv

    @convert_input
    def __truediv__(self, other):
        rv = Variable(self.value / other.value, op="mull")
        rv.prev.extend([self, other])
        self.next.append(rv)
        rv.derivative = lambda x: x * (1. / other.value)
        return rv

    def __pow__(self, power, modulo=None):
        rv = Variable(self.value ** power, op="pow")
        rv.prev.append(self)
        self.next.append(rv)
        rv.derivative = lambda x: x * power * (self.value) ** (power - 1)
        return rv

    def relu(self):
        rv = Variable(max(0, self.value), op="relu")
        rv.prev.extend([self])
        self.next.append(rv)
        rv.derivative = lambda x: x * 1 if self.value >= 0 else 0
        return rv

    def sigmoid(self):
        _sigmoid = lambda x : 1./(1.+math.exp(-x))
        rv = Variable(_sigmoid(self.value), op="sigmoid")
        rv.prev.extend([self])
        self.next.append(rv)
        rv.derivative = lambda x: x * _sigmoid(self.value) * (1-_sigmoid(self.value))
        return rv

    def __str__(self):
        return str(self.value)

    def backward(self):
        for prev in self.prev:
            if prev is not None:
                if self.derivative is not None:
                    prev.grad = self.derivative(self.grad)
                prev.backward()
